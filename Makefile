##################
# Variables
##################

DOCKER_COMPOSE = docker-compose -f ./backend/docker/docker-compose.yml

##################
# Docker compose
##################

app_build:
	cp ./backend/.env.example ./backend/.env
	cp ./backend/docker/.env.dist ./backend/docker/.env
	${DOCKER_COMPOSE} build
	${DOCKER_COMPOSE} up -d --remove-orphans
	${DOCKER_COMPOSE} exec php-fpm composer install
	cd ./frontend && yarn install && yarn run dev