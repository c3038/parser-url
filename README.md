## Парсер картинок по url

### Описание:

Приложение анализирует указанную пользователем
веб-страницу, извлекать все изображения и отображать 
их в удобном виде. Также необходимо рассчитать суммарный
размер всех изображений и предоставить эту информацию в
отчете.

### Функциональные требования

- Экран 1:
Поле ввода URL: Пользователь должен иметь возможность ввести URL веб-страницы.
Кнопка “Го”: При нажатии на кнопку, приложение должно перейти к следующему экрану и начать обработку указанного URL.

- Экран 2:
Вывод результата в виде сетки: Необходимо вывести найденные на указанной странице изображения в виде сетки (4 изображения в строке).
Отчет под таблицей изображений: Под таблицей необходимо предоставить отчет, который будет содержать количество обнаруженных изображений и их суммарный размер в мегабайтах. Формат отчета: “На странице обнаружено [указать результат] изображений на [указать размер] Мб”.

###  Подход к реализации

- frontend:
    SPA(Single Page Application)
- backend:
    DDD(domain-driven design)

### Стек
- frontend:
    Vue, Vuex, Vue-router, axios, vite
- backend:
  Symfony, Docker

## Установка

```bash
git clone 
git checkout parser-url
```

****
### Linux

```bash
make app_build
```
****
Windows
```bash
cp ./backend/.env.example ./backend/.env
cp ./backend/docker/.env.dist ./backend/docker/.env
docker-compose build
docker-compose up -d --remove-orphans
docker-compose exec php-fpm composer install
cd ./frontend && yarn install && yarn run dev
```

## API

Base URL: **/api/v1**

### **POST** /images

Парсит картинки со страницы по url в параметре "url"

Параметры:

```json
{
  "url": "https://vuex.vuejs.org/"
}

```
Ответ:


```json
{
  "totalImageSize": 0.03,
  "totalImageCount": 2,
  "imagesUrl": [
    //ссылки на картинки
  ]
}

```
или

```json
{
    "error": //код ошибки
    "message": // описание ошибки
}
```