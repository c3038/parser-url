import { createStore } from 'vuex';

const store = createStore({
    state() {
        return {
            images: {
                urls: [],
                sizeTotal: 0,
                count: 0,
            }
        };
    },
    actions: {
       SET_IMAGES({commit}, data) {
           commit('SET_IMAGES_URLS_TO_SATE', data);
       }
    },

    mutations: {
        SET_IMAGES_URLS_TO_SATE(state, data) {
            state.images = {
                urls: data.imagesUrl,
                sizeTotal: data.totalImageSize,
                count: data.totalImageCount,
            }
        }
    },
    getters: {
        IMAGES(state) {
            return state.images;
        }
    }
});

export default store;
