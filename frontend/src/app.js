import {createApp} from 'vue';
import Index from './components/Index.vue'
import Router from "./router/router.js";
import Store from "./store/store.js";

import './bootstrap';

export const app = createApp(Index)
    .use(Router)
    .use(Store)
    .mount('#app');
