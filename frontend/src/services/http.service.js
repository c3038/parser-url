import axios from "axios";
import configFile from "../config.json";
import NProgress from 'nprogress'
import 'nprogress/nprogress.css';

const http = axios.create({
  baseURL: configFile.apiEndpoint
});

http.interceptors.request.use(config => {
    NProgress.start()
    return config
})

http.interceptors.response.use(response => {
    NProgress.done()
    return response
})

const httpService = {
  get: http.get,
  post: http.post,
  put: http.put,
  delete: http.delete,
  patch: http.patch
};
export default httpService;
