import Home from "@/pages/Home.vue";
import ImageList from "@/pages/ImageList.vue";


const routes = [
    {
        path: '/',
        component: Home
    },
    {
        path: '/image-list',
        name: 'imagesList',
        component: ImageList,
    },
];

export default routes;


