<?php

declare(strict_types=1);

namespace App\Controller;

use App\API\Application\Contract\CreateImagesListInterface;
use App\API\Domain\ValueObject\ParsePath;
use App\API\Infrastructure\Http\Output\HttpErrorMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/v1/images', name: 'images', methods: ['POST'])]
class ImageController extends AbstractController
{
    private CreateImagesListInterface $imagesList;

    public function __construct(CreateImagesListInterface $imagesList)
    {
        $this->imagesList = $imagesList;
    }

    /**
     *     * @throws TransportExceptionInterfaces
     *     * @throws InvalidArgumentException
     */
    public function __invoke(#[MapRequestPayload] ParsePath $parsePath
    ): JsonResponse
    {
        try {
            return (new JsonResponse($this->imagesList->create($parsePath)))->setStatusCode(Response::HTTP_OK);
        } catch (\Throwable $e) {
            $errorMessage = new HttpErrorMessage();
            $errorMessage->error = $e->getCode();
            $errorMessage->message = $e->getMessage();
            return (new JsonResponse($errorMessage))->setStatusCode($errorMessage->error);
        }
    }
}