<?php

declare(strict_types=1);

namespace App\API\Application\Contract;

use App\API\Domain\Entity\ImagesList;
use App\API\Domain\ValueObject\ParsePath;

interface CreateImagesListInterface
{
    public function create(ParsePath $dto): ImagesList;
}