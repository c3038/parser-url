<?php

declare(strict_types=1);

namespace App\API\Application\UseCase;

use App\API\Application\Contract\CreateImagesListInterface;
use App\API\Domain\Contract\LoadImagesListRepositoryInterface;
use App\API\Domain\Entity\ImagesList;
use App\API\Domain\ValueObject\ParsePath;

class CreateImagesList implements CreateImagesListInterface
{
    private LoadImagesListRepositoryInterface $repository;

    public function __construct(LoadImagesListRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(ParsePath $dto): ImagesList
    {
        $result = new ImagesList();
        $result->imagesUrl = $this->repository->getListImagesUrls($dto->url);
        $result->totalImageCount = count($result->imagesUrl);
        $result->totalImageSize = round(
            $this->repository->getTotalSizeImages($result->imagesUrl) / (1024 * 1024),
            2
        );

        return $result;
    }
}
