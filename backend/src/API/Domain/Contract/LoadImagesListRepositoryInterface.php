<?php

declare(strict_types=1);

namespace App\API\Domain\Contract;

interface LoadImagesListRepositoryInterface
{
    public function getListImagesUrls(string $url): array;

    public function getTotalSizeImages(array $urls): float;
}