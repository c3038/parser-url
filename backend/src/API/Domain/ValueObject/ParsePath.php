<?php

declare(strict_types=1);

namespace App\API\Domain\ValueObject;

use Symfony\Component\Validator\Constraints as Assert;

class ParsePath
{
    public function __construct(
        #[Assert\Url(
            message: 'ParsePath {{ value }} должен быть валидным!',
        )]
        readonly public string $url,
    ) {
    }
}