<?php

declare(strict_types=1);

namespace App\API\Domain\Entity;

class ImagesList
{
    public float $totalImageSize;
    public int $totalImageCount;
    public array $imagesUrl;

}