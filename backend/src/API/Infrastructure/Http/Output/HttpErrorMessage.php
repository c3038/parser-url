<?php

declare(strict_types=1);

namespace App\API\Infrastructure\Http\Output;

use Symfony\Component\HttpFoundation\Response;
class HttpErrorMessage
{
    public int $error;
    public string $message;
}