<?php

declare(strict_types=1);

namespace App\API\Infrastructure\Repository;

use App\API\Domain\Contract\LoadImagesListRepositoryInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LoadImagesListRepository implements LoadImagesListRepositoryInterface
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getListImagesUrls(string $url): array
    {

        $response = $this->client->request('GET', $url);

        $dom = new \DOMDocument();

        preg_match('/^https?:\/\/.*\//', $url, $matches);
        $urlBase = substr($matches[0], 0, -1);

        libxml_use_internal_errors(true);
        $dom->loadHTML($response->getContent());
        libxml_use_internal_errors(false);

        $images = $dom->getElementsByTagName('img');

        foreach ($images as $image) {
            $src = $image->getAttribute('src');
            if (!empty($src)) {
                $urls[] = preg_match('/^https?:\/\/.*/', $src)
                    ? $src
                    : sprintf('%s%s', $urlBase, $src);
            }
        }

        return $urls ?? [];
    }

    public function getTotalSizeImages(array $urls): float
    {
        $totalImageSize = 0;

        foreach ($urls as $url) {
            $responses[] = $this->client->request('HEAD', $url);
        }

        foreach ($this->client->stream($responses) as $response => $chunk) {
            if ($chunk->isFirst()) {
                $headers = $response->getHeaders();
                if (isset($headers['content-length'])) {
                    $totalImageSize += (int)$headers['content-length'][0];
                }
            }
        }
        return $totalImageSize;
    }
}